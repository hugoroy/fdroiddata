Categories:System
License:ISC
Web Site:https://github.com/yvesf/andiodine
Source Code:https://github.com/yvesf/andiodine
Issue Tracker:https://github.com/yvesf/andiodine/issues
Bitcoin:114WP9Pnr97ZwgGmBz2g4QYPsqyLz5qRSo

Auto Name:AndIodine
Summary:Data connection through DNS tunneling
Description:
Andiodine lets you tunnel IPv4 data through a DNS server. This can be
usable in different situations where internet access is firewalled,
but DNS queries are allowed. 

Based on iodine, requires Android4+.
.

Repo Type:git
Repo:https://github.com/yvesf/andiodine.git

Build:1.0,1
    commit=1ae6623962ab115457deb00a9f40405627c4fdc4
    buildjni=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

