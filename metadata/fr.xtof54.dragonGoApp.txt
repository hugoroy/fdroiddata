Categories:Games
License:AGPL
Web Site:https://github.com/cerisara/DragonGoApp
Source Code:https://github.com/cerisara/DragonGoApp
Issue Tracker:https://github.com/cerisara/DragonGoApp/issues

Auto Name:DragonGoApp
Summary:Go game for DGS
Description:
Open-source android App to play the game of go on the DGS server.

DragonGoApp features:

* Minimize bandwidth usage: this is especially useful when you have a limited quota of 3G data connection
* Full-featured SGF editor/player based on the open-source eidogo code. Also shows game information and game comments
* Support dual servers/credentials (DGS and develDGS)
* Play moves, pass, resign, send/receive messages, attach a comment to a move
* Invite/accept/refuse challenges, challenge in 19x19 ladder
* Follow reviews of professional games from IGS, which are included in the app
.

Repo Type:git
Repo:https://github.com/cerisara/DragonGoApp

Build:1.0,1
    commit=68e3c8ea9f4

Build:1.2,2
    commit=3d133d3489a

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2
Current Version Code:2

