Categories:Internet
License:GPLv2
Web Site:https://code.google.com/p/ics-openvpn
Source Code:https://code.google.com/p/ics-openvpn/source
Issue Tracker:https://code.google.com/p/ics-openvpn/issues

Auto Name:OpenVPN for Android
Summary:OpenVPN without root
Description:
With the VPNService in Android 4.0+ it is possible to create a VPN that doesn't
need root access.
.

Repo Type:hg
Repo:https://code.google.com/p/ics-openvpn

Build:0.5.21,48
    disable=broken v0.5.21
    commit=unknown - see disabled
    buildjni=yes

Build:0.5.22,49
    disable=broken v0.5.22
    commit=unknown - see disabled
    buildjni=yes

Build:0.5.24,51
    disable=broken too v0.5.24
    commit=unknown - see disabled
    target=android-14
    prebuild=sed -i 's/ndk-build APP_API=all -j 8/ndk-build APP_ABI=all -j 2/g' build-native.sh && \
        ./build-native.sh
    buildjni=no

Build:0.5.35,63
    disable=broken too v0.5.35
    commit=unknown - see disabled
    target=android-17
    prebuild=sed -i 's/-j 8/-j 2/g' build-native.sh && \
        ./build-native.sh
    buildjni=no

Build:0.5.47,80
    commit=v0.5.47
    gradle=yes
    rm=assets
    prebuild=sed -i '/google-breakpad/d' jni/Android.mk && \
        sed -i -e '/+=.*breakpad/d' -e 's/ [^ ]*breakpad[^ ]*//' openvpn/Android.mk && \
        find openvpn/src -type f -print0 | xargs -0 sed -i '/breakpad/d' && \
        rm -f openvpn/src/openvpn/breakpad.*
    build=echo WITH_BREAKPAD=0 >> jni/Android.mk && \
        sed -i 's/-j 8/-j 4/g' misc/build-native.sh && \
        ./misc/build-native.sh
    buildjni=no

Build:0.6.0,81
    commit=v0.6.0
    gradle=yes
    prebuild=sed -i '/google-breakpad/d' jni/Android.mk && \
        sed -i -e '/+=.*breakpad/d' -e 's/ [^ ]*breakpad[^ ]*//' openvpn/Android.mk && \
        find openvpn/src -type f -print0 | xargs -0 sed -i '/breakpad/d' && \
        rm -f openvpn/src/openvpn/breakpad.* && \
        mv src/de/blinkt/openvpn/fragments/SeekbarTicks.java src/de/blinkt/openvpn/fragments/SeekBarTicks.java
    scandelete=assets
    build=echo WITH_BREAKPAD=0 >> jni/Android.mk && \
        sed -i 's/-j 8/-j 4/g' misc/build-native.sh && \
        ./misc/build-native.sh
    buildjni=no

Build:0.6.1,82
    commit=v0.6.1
    gradle=yes
    prebuild=sed -i '/google-breakpad/d' jni/Android.mk && \
        sed -i -e '/+=.*breakpad/d' -e 's/ [^ ]*breakpad[^ ]*//' openvpn/Android.mk && \
        find openvpn/src -type f -print0 | xargs -0 sed -i '/breakpad/d' && \
        rm -f openvpn/src/openvpn/breakpad.*
    scandelete=assets
    build=echo WITH_BREAKPAD=0 >> jni/Android.mk && \
        sed -i 's/-j 8/-j 4/g' misc/build-native.sh && \
        ./misc/build-native.sh
    buildjni=no

Build:0.6.5,86
    commit=v0.6.5
    gradle=yes
    prebuild=sed -i '/google-breakpad/d' jni/Android.mk && \
        sed -i -e '/+=.*breakpad/d' -e 's/ [^ ]*breakpad[^ ]*//g' openvpn/Android.mk && \
        find openvpn/src -type f -print0 | xargs -0 sed -i '/breakpad/d' && \
        rm -f openvpn/src/openvpn/breakpad.*
    scandelete=assets
    build=echo WITH_BREAKPAD=0 >> jni/Android.mk && \
        sed -i 's/-j 8/-j 4/g' misc/build-native.sh && \
        sed -e '/HAVE_CONFIG_VERSION_H/ d' -i openvpn/config.h && \
        ./misc/build-native.sh
    buildjni=no

Build:0.6.9a,90
    commit=v0.6.9a-production
    subdir=main
    gradle=yes
    prebuild=sed -i -e 's/WITH_BREAKPAD=.*/WITH_BREAKPAD=0/' -e '/include google-breakpad/d' jni/Android.mk
    build=./misc/build-native.sh
    buildjni=no

Build:0.6.10,91
    commit=v0.6.10-production
    subdir=main
    gradle=yes
    build=./misc/build-native.sh USE_BREAKPAD=0
    buildjni=no

Build:0.6.11,92
    commit=v0.6.11-production
    subdir=main
    gradle=normal
    build=./misc/build-native.sh USE_BREAKPAD=0
    buildjni=no

Maintainer Notes:
This has (as at 0.6.11) openssl 1.0.0e source code embedded in the source
repo. It is not vulnerable to CVE-2014-0160.
.

Auto Update Mode:None
Update Check Mode:Tags .*-production
Current Version:0.6.11
Current Version Code:92

