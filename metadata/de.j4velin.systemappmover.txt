Categories:System
License:Apache2
Web Site:https://github.com/j4velin/SystemAppMover
Source Code:https://github.com/j4velin/SystemAppMover
Issue Tracker:https://github.com/j4velin/SystemAppMover/issues

Auto Name:/system/app mover
Summary:Add and remove system apps
Description:
This app moves apps from and to the /system/app folder, making them a system
app or a user app. System apps can get more priviledges, so some apps get more
functionality when installed as a system app.

On the other hand, system apps can not be uninstalled. So this app can also be
used to convert system apps to normal user apps by moving them from the
/system/app directory to /data/app directory.

WARNING: Uninstalling important system apps might result in a unusable device!
Use this function at your own risk and only if you know what you're doing!

This app requires a rooted device with BusyBox installed.
.

Requires Root:Yes

Repo Type:git
Repo:https://github.com/j4velin/SystemAppMover.git

Build:1.5.6,19
    commit=457e6d661b726d5
    target=android-19
    srclibs=1:RootTools@253

Build:1.5.7,20
    commit=7c449eb05bbcf
    target=android-19
    srclibs=1:RootTools@253

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.5.7
Current Version Code:20

