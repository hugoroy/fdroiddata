Categories:Office
License:GPLv3
Web Site:https://github.com/schildbach/bitcoin-wallet
Source Code:https://github.com/schildbach/bitcoin-wallet
Issue Tracker:https://github.com/schildbach/bitcoin-wallet/issues
Bitcoin:1HkX6X8EakdsgAysL93oKrWiXGPbFiG1xV

Auto Name:Bitcoin
Summary:Store digital currency
Description:
* No cloud server or web service needed: this wallet works peer to peer
* Display of wallet balance in Bitcoin and various other currencies
* Sending and receiving of Bitcoin via NFC, QR-codes or Bitcoin URLs
* Address book for regularly used Bitcoin addresses
* Enter transactions while offline, will be executed when online
* Manages blockchain on your device
* System notifications for Bitcoin connectivity and received coins
* App widget for Bitcoin balance
* Back up private keys to local storage encrypted with a password

A few tens of MiB must be downloaded to set up the blockchain and a couple
more per month subsequently to keep it updated.

See the
[http://bitcoin-wallet.googlecode.com/git/wallet/README Readme] for more
details. Use this at your own risk and be wary that if you don't make a
backup of the private keys you will no longer be able to acces the Bitcoins
contained in the wallet, should the data of the app be lost.

There's a separate version of this application that uses a test version of the
Bitcoin network: [[de.schildbach.wallet_test]].

[https://raw.github.com/schildbach/bitcoin-wallet/master/wallet/CHANGES Changelog]
.

# BitcoinJWallet (author's fork) is rebased each version so old commits wont work
# patches post 3.0 will also need re-doing
Repo Type:git
Repo:https://github.com/schildbach/bitcoin-wallet.git

#Note: there used to be build data for 1.48.2/66 but the source code for this
#seems to have gone (it was in a maintenance branch) when the project went
#from svn to git.
Build:2.01,74
    commit=a55ef6822c9e
    subdir=wallet
    init=sed -i 's/_test//' AndroidManifest.xml && \
        find src -name *.java -exec sed -i 's/_test\.R/.R/' {} \;
    maven=yes
    srclibs=BitcoinJWallet@31953d239555
    prebuild=mvn install:install-file -DgroupId=android -DartifactId=android -Dversion=2.3.3_r2 -Dpackaging=jar -Dfile=$$SDK$$/platforms/android-10/android.jar && \
        mvn install:install-file -DgroupId=android.support -DartifactId=compatibility-v4 -Dversion=r6 -Dpackaging=jar -Dfile=$$SDK$$/android-compatibility/v4/android-support-v4.jar

Build:2.16,87
    commit=49c3da045e57
    subdir=wallet
    init=sed -i 's/_test//' AndroidManifest.xml && \
        find src -name *.java -exec sed -i 's/_test\.R/.R/' {} \;
    maven=2
    srclibs=BitcoinJWallet@a160dc24f38c
    prebuild=cd ../integration-android && \
        mvn3 install && \
        cd ../wallet && \
        mvn3 install:install-file -DgroupId=android -DartifactId=android -Dversion=2.3.3_r2 -Dpackaging=jar -Dfile=$$SDK$$/platforms/android-10/android.jar && \
        mvn3 install:install-file -DgroupId=android.support -DartifactId=compatibility-v4 -Dversion=r6 -Dpackaging=jar -Dfile=$$SDK$$/android-compatibility/v4/android-support-v4.jar

Build:3.01,121
    commit=742241f59df1
    maven=yes
    srclibs=BitcoinJWallet@1649520e3183
    patch=pom.patch
    prebuild=sed -i '54 i \\tandroid:debuggable=\"false\"' wallet/AndroidManifest.xml && \
        cp -r $$BitcoinJWallet$$/ . && \
        rm -rf market/

Build:3.04,124
    commit=3efa5beb206e
    maven=yes
    srclibs=BitcoinJWallet@7cb7bfc11c11
    patch=pom.patch
    prebuild=cp -r $$BitcoinJWallet$$/ . && \
        rm -rf market/

# source plugin causes an error after building:
# W/asset   (16561) Asset path target/library-4.1.0.apk is neither a directory nor file (type=1).
Build:3.14,134
    commit=5ede628245a8
    maven=yes
    srclibs=BitcoinJWallet@67b187c4c4c4
    prebuild=sed -i '30,33d' pom.xml && \
        cp -r $$BitcoinJWallet$$/ . && \
        rm -rf market/
    build=$$MVN3$$ clean install -f BitcoinJWallet/pom.xml

# important bugfix so uses the original BitcoinJ
Build:3.15,136
    commit=dd70948b66aa
    maven=yes
    srclibs=BitcoinJWallet-origin@777e6781d
    prebuild=sed -i '30,33d' pom.xml && \
        sed -i 's/0.11-SNAPSHOT/0.10/g' wallet/pom.xml && \
        cp -r $$BitcoinJWallet-origin$$/ . && \
        rm -rf market/
    build=$$MVN3$$ clean install -DskipTests -f BitcoinJWallet-origin/pom.xml

Build:3.19,140
    commit=885f13e01a86
    maven=yes
    srclibs=BitcoinJWallet@1134572f61
    prebuild=sed -i '30,33d' pom.xml && \
        cp -r $$BitcoinJWallet$$/ . && \
        rm -rf market/
    build=$$MVN3$$ clean install -DskipTests -f BitcoinJWallet/pom.xml

Build:3.22,143
    commit=8e67f8ea6e38
    subdir=wallet
    maven=yes@..
    srclibs=BitcoinJWallet@v0.10.2
    prebuild=cp -r $$BitcoinJWallet$$/ . && \
        rm -rf ../market/ && sed -i '/sample-integration-android/d' ../pom.xml
    build=$$MVN3$$ clean install -DskipTests -f BitcoinJWallet/pom.xml

Build:3.27,148
    commit=5787167f05b8
    subdir=wallet
    maven=yes@..
    rm=market/
    srclibs=BitcoinJWallet-origin@v0.10.3
    prebuild=cp -r $$BitcoinJWallet-origin$$/ BitcoinJWallet
    build=$$MVN3$$ clean install -f BitcoinJWallet/pom.xml

Build:3.28,149
    commit=f796d9531f2d
    subdir=wallet
    maven=yes@..
    rm=market/
    srclibs=BitcoinJWallet-origin@v0.10.3
    prebuild=cp -r $$BitcoinJWallet-origin$$/ BitcoinJWallet
    build=$$MVN3$$ clean install -f BitcoinJWallet/pom.xml

Build:3.39,160
    commit=9b292a1f4ff30aefbd
    subdir=wallet
    maven=yes@..
    srclibs=BitcoinJWallet@v0.11.1
    prebuild=cp -r $$BitcoinJWallet$$/ BitcoinJWallet
    build=$$MVN3$$ clean install -DskipTests -f BitcoinJWallet/pom.xml

Build:3.46,167
    commit=a38ec5e1d9b6569ae3
    subdir=wallet
    maven=yes@..
    srclibs=BitcoinJWallet@v0.11.2
    build=$$MVN3$$ clean install -DskipTests -f $$BitcoinJWallet$$/pom.xml

Maintainer Notes:
See wallet/{README,CHANGES} or git log for info about what BitcoinJ tag to use.
Use latest commit in 'prod' branch, which is a rebase on top of the latest tag.

Ensure the developer has remembered to update name/icon in prod branch (as did
not happen with 3.23, which appears to be why it was quickly replaced with 3.24.
(For the same reason, there is no 3.24_test)

Note also that the 'prod:' commits seem to disappear, as if the developer is
removing them and force-pushing the branch. We need to take an alternative
approach to building this in future.

Update the _test app as well.
.

Auto Update Mode:None
Update Check Mode:RepoManifest/prod
Current Version:3.46
Current Version Code:167

