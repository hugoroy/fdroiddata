Categories:System
License:MIT
Web Site:http://trikita.co/#obsqr
Source Code:https://bitbucket.org/trikita/obsqr/src
Issue Tracker:https://bitbucket.org/trikita/obsqr/issues?status=new&status=open

Auto Name:Obsqr
Summary:QR code scanner
Description:
Fast and simple QR code scanner that uses the zbar library.
Minimalistic design allows you to access QR content with a single tap.

'''N.B''' QR codes only, not other kinds of barcodes.
.

Repo Type:hg
Repo:https://bitbucket.org/trikita/obsqr

Build:2.4,9
    commit=071341595ce3
    target=android-15
    prebuild=bash fetch-zbar.sh
    buildjni=yes

Build:2.5,10
    commit=4c6ad70039ba
    target=android-16
    prebuild=bash fetch-zbar.sh
    buildjni=yes

Build:2.6,11
    commit=0b81963
    target=android-19
    prebuild=bash fetch-zbar.sh
    buildjni=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.6
Current Version Code:11

