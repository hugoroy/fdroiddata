Disabled:only publish when provider that doesn't require API keys is available or we have an ssh or rsync app in the repository v1.0
Categories:System
License:GPLv3
Web Site:https://github.com/mrpdaemon/encdroid
Source Code:https://github.com/mrpdaemon/encdroid
Issue Tracker:https://github.com/mrpdaemon/encdroid/issues

Auto Name:Encdroid
Summary:Access encrypted files
Description:
No description available
.

Repo Type:git
Repo:https://github.com/mrpdaemon/encdroid.git

Build:1.0,1
    disable=see Disabled
    commit=dummy
    target=android-13
    extlibs=dropbox-sdk/dropbox-android-sdk-1.5.1.jar,httpmime/httpmime-4.0.3.jar,json-simple/json_simple-1.1.jar
    srclibs=EncfsJava@v0.1
    prebuild=mvn package -f $$EncfsJava$$/pom.xml && \
        cp $$EncfsJava$$/target/encfs-java-0.1.jar libs/

Maintainer Notes:
In the event that this is ever enabled, also check that the openssl source
(it has its own copy embedded in the source repo) is not vulnerable to
CVE-2014-0160.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0.3
Current Version Code:1400203

