Categories:System
License:GPLv3
Web Site:http://sites.google.com/site/ghostcommander1
Source Code:http://sourceforge.net/p/ghostcommander/code
Issue Tracker:http://sourceforge.net/projects/ghostcommander/support
Donate:http://sourceforge.net/p/ghostcommander/donate

Auto Name:Ghost Commander
Summary:Dual-panel file manager
Description:
Dual panel file manager, like Norton Commander, Midnight Commander or
Total Commander. Unlike regular file managers, it copies and moves
files between its two panels.

The file manager can also create or extract ZIP archives and transfer
files to/from FTP servers.
The SFTP/SMB network share functionalities need
plugin apps, that can also be found in the repository.
There is also a handy app manager featuring package names,
installation dates, sharing apks (via long-press), manifests and
access to activities and shortcuts.

Requires root: No, but if you allow it root you can remount filesystems,
and mess around with the system files.
.

Repo Type:srclib
Repo:GhostCommander

Build:1.35.1b5,97
    commit=141
    prebuild=sed -ri 's/(debuggable)="true"/\1="false"/' AndroidManifest.xml

# All commit messages are empty, so I'm just guessing the revision.
Build:1.35,94
    commit=131
    prebuild=sed -ri 's/(debuggable)="true"/\1="false"/' AndroidManifest.xml

Build:1.36.4,110
    commit=161
    prebuild=sed -ri 's/(debuggable)="true"/\1="false"/' AndroidManifest.xml

Build:1.40,160
    commit=302
    prebuild=sed -ri 's/(debuggable)="true"/\1="false"/' AndroidManifest.xml

Build:1.40.1,163
    commit=307

Build:1.40.2,164
    commit=314

# DexClassLoader seems to be used for the samba/sftp plugins
Build:1.43.3b1,210
    commit=407
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.50.1,214
    commit=387
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51b2,219
    commit=390
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51b6,223
    commit=398
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51b7,224
    commit=402
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51.4,233
    disable=Build fails - missing R.drawable.plugins
    commit=412
    scanignore=src/com/ghostsq/commander/adapters/CA.java

Build:1.51.5b3,236
    disable=skip beta

Maintainer Notes:
No commit messages in source repo! No tags. Apks on website can be used to
confirm a release is real. Commit where AndroidManifest is changed usually
corresponds to version.

The scanignore is for the dex class loading which is used for loading plugins.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.52.1
Current Version Code:246

