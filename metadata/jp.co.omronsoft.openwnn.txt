Categories:Office
License:Apache2
Web Site:
Source Code:https://android.googlesource.com/platform/packages/inputmethods/OpenWnn
Issue Tracker:

Name:OpenWnn
Auto Name:OpenWnn
Summary:Japanese Input method
Description:
Japanese keyboard/IME from the Android Open Source Project.

Status: it may not be possible to build future versions as they rely
on resources that are not in the Android SDK.
.

Repo Type:git
Repo:https://android.googlesource.com/platform/packages/inputmethods/OpenWnn

Build:1.3.5,1
    disable=buggy at android-4.0.4_r2.1
    commit=android-4.0.4_r2.1
    target=android-14
    prebuild=sed -i -e '18 s/>//g' -e '19 a android:versionCode="1"\nandroid:versionName="1.3.5">'            AndroidManifest.xml && \
        sed -i '23 i <uses-sdk android:minSdkVersion="8" android:targetSdkVersion="14"/>'            AndroidManifest.xml
    build=$$NDK$$/ndk-build APP_BUILD_SCRIPT=Android.mk

Build:1.3.5.1,2
    disable=causes same crash as above - android-4.0.4_r2.1
    commit=unknown - see disabled
    target=android-14
    patch=styles_2.patch
    build=$$NDK$$/ndk-build APP_BUILD_SCRIPT=Android.mk

Build:1.3.5.2,3
    commit=android-4.0.4_r2.1
    target=android-14
    patch=styles_3.patch
    build=$$NDK$$/ndk-build APP_BUILD_SCRIPT=Android.mk

Build:1.3.6,136
    disable=https://f-droid.org/forums/topic/opewnn-ics at android-4.1.2_r2
    commit=android-4.1.2_r2
    target=android-15
    prebuild=sed -i ' 18 i android:versionCode="136"\nandroid:versionName="1.3.6"'            AndroidManifest.xml && \
        sed -i '22 i <uses-sdk android:minSdkVersion="8" android:targetSdkVersion="15" />'            AndroidManifest.xml
    buildjni=libs

Auto Update Mode:None
Update Check Mode:Static
Current Version:1.3.5.2
Current Version Code:3

