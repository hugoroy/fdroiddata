Categories:System
License:Apache2
Web Site:
Source Code:https://github.com/eolwral/OSMonitor
Issue Tracker:https://github.com/eolwral/OSMonitor/issues
Donate:https://github.com/eolwral/OSMonitor

Auto Name:OS Monitor
Summary:Monitor the Operating System
Description:
* Process: monitor all processes and display detail information about each.
* Connection: display every tcp or udp network connection, and query it via WHOIS. There is an option to display the reverse-ip lookup map in OpenStreetMap.
* Misc: monitor processor, battery and file system.
* Message: check dmesg or logcat in real-time.

[https://github.com/eolwral/OSMonitor/blob/master/CHANGELOG.md Changelog]
.

Repo Type:git
# For 2.0.5
#Repo:https://code.google.com/p/android-os-monitor.osmonitor
Repo:https://github.com/eolwral/OSMonitor

Build:2.0.5,28
    commit=cee4327bfe56
    subdir=OSMonitor
    patch=no-gmaps.patch,name-no-gmaps.patch
    buildjni=yes

Build:3.0.6.4,41
    commit=667a709a0
    subdir=OSMonitor
    encoding=latin1
    srclibs=1:ActionBarSherlock@4.3.1
    prebuild=mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        sed -i 's/osinfo/osInfo/g' jni/include/core/os.h && \
        sed -i 's/ConnectionInfo.pb.h/connectionInfo.pb.h/g' jni/include/core/connection.h
    build=$$NDK$$/ndk-build && \
        mv libs/armeabi/osmcore assets/osmcore_arm && \
        mv libs/x86/osmcore assets/osmcore_x86
    buildjni=no

Build:3.0.6.5,42
    commit=330fc16
    subdir=OSMonitor
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.3.1
    prebuild=mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    build=$$NDK$$/ndk-build && \
        mv libs/armeabi/osmcore assets/osmcore_arm && \
        mv libs/x86/osmcore assets/osmcore_x86
    buildjni=no

Build:3.0.6.6,43
    commit=3.0.6.6
    subdir=OSMonitor
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.3.1
    prebuild=mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    build=$$NDK$$/ndk-build && \
        mv libs/armeabi/osmcore assets/osmcore_arm && \
        mv libs/x86/osmcore assets/osmcore_x86
    buildjni=no

Build:3.0.7.0,48
    disable=doesn't show any data
    commit=3.0.7.0
    subdir=OSMonitor
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    build=$$NDK$$/ndk-build && \
        mv libs/armeabi/osmcore assets/osmcore_arm && \
        mv libs/x86/osmcore assets/osmcore_x86
    buildjni=no

Build:3.0.7.2,50
    commit=3.0.7.2
    subdir=OSMonitor
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/
    build=$$NDK$$/ndk-build && \
        mv libs/armeabi/osmcore assets/osmcore_arm && \
        mv libs/x86/osmcore assets/osmcore_x86
    buildjni=no

Build:3.0.8.2,53
    commit=3.0.8.2
    subdir=OSMonitor
    extlibs=android/android-support-v4.jar
    srclibs=1:Support/v7/appcompat@android-4.4_r1.1,2:Volley@android-4.4_r1.2
    prebuild=mv libs/android-support-v4.jar $$Support$$/libs/
    scanignore=OSMonitor/assets/osmcore_arm
    build=$$NDK$$/ndk-build && \
        mv libs/armeabi/osmcore assets/osmcore_arm && \
        mv libs/x86/osmcore assets/osmcore_x86
    buildjni=no

Build:3.0.9.0,54
    commit=3.0.9.0
    subdir=OSMonitor
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=1:Support/v7/appcompat@android-4.4_r1.1,2:Volley@android-4.4_r1.2,3:ColorPicker-eolwral@788621da0c
    prebuild=mv libs/android-support-v4.jar $$Support$$/libs/ && \
        mv jni jni_
    build=mv jni_ jni && \
        $$NDK$$/ndk-build && \
        mkdir -p assets && \
        mv libs/armeabi/osmcore assets/osmcore_arm && \
        mv libs/mips/osmcore assets/osmcore_mips && \
        mv libs/x86/osmcore assets/osmcore_x86

Maintainer Notes:
Could support additional CPU architectures and lose the scanignore by building
the stuff in /jni and moving the three outputs to /assets.

Once jni builds properly and there is a new release out, do:

Build:3.0.9.3,57
    commit=...
    subdir=OSMonitor
    extlibs=android/android-support-v4.jar
    srclibs=1:Support/v7/appcompat@android-4.4_r1.1,2:Volley@android-4.4_r1.2,3:ColorPicker-eolwral@788621da0c
    prebuild=mv libs/android-support-v4.jar $$Support$$/libs/
    buildjni=yes
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.0.9.0
Current Version Code:54

