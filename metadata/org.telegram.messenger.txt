AntiFeatures:NonFreeNet
Categories:Internet
License:GPLv2+
Web Site:https://telegram.org
Source Code:https://github.com/slp/Telegram-FOSS
Issue Tracker:https://github.com/slp/Telegram-FOSS/issues

Auto Name:Telegram
Summary:Messaging client
Description:
Client for the Telegram messaging platform. Chat with friends, start group
chats and share all kinds of content. All of your messages and conversations
are stored in Telegram's cloud.

The messaging platform is aimed at mobile devices, but desktop and web clients
exist as well.

Several proprietary parts were removed from the original Telegram client,
including Google Play Services for the location services and HockeySDK for
self-updates. Push notifications through Google Cloud Messaging and the
automatic SMS receiving features were also removed.

Anti-Feature: Non-Free Network, since the servers run proprietary software.
.

Repo Type:git
Repo:https://github.com/slp/Telegram-FOSS.git

Build:1.3.21,160
    commit=v1.3.21a
    subdir=TMessagesProj
    gradle=yes
    build=ndk-build && \
        gradle nativeLibsToJar
    buildjni=no

Build:1.3.26,177
    commit=v1.3.26a
    subdir=TMessagesProj
    gradle=yes
    build=ndk-build && \
        gradle nativeLibsToJar
    buildjni=no

Build:1.4.9,212
    commit=v1.4.9a
    subdir=TMessagesProj
    gradle=yes
    patch=jni_includes.patch
    build=ndk-build && \
        gradle nativeLibsToJar
    buildjni=no

Build:1.4.9c,219
    commit=v1.4.9c
    subdir=TMessagesProj
    gradle=yes
    forceversion=yes
    patch=jni_includes.patch
    build=ndk-build && \
        gradle nativeLibsToJar
    buildjni=no

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.4.9c
Current Version Code:219

