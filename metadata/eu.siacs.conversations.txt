Categories:Internet
License:GPLv3
Web Site:https://github.com/siacs/Conversations
Source Code:https://github.com/siacs/Conversations
Issue Tracker:https://github.com/siacs/Conversations/issues

Auto Name:Conversations
Summary:XMPP client
Description:
XMPP client designed with ease of use and security in mind.

Features:
* end-to-end encryption with either OTR or openPGP
* Holo UI
* syncs with your desktop client
* group chats
* addressbook integration
* multiple accounts / unified inbox

Noteable XEPs:
* XEP-0198: Stream Management
* XEP-0280: Message Carbons
* XEP-0237: Roster Versioning

This app uses precompiled binary libraries verified to match their
upstream hashes.
.

Repo Type:git
Repo:https://github.com/siacs/Conversations.git

Build:0.1.3,9
    commit=0.1.3
    submodules=yes
    rm=libs/android-support-v4.jar
    extlibs=android/android-support-v4.jar
    srclibs=Otr4j-jitsi@33d95bb3710986d048fb4cbe42dc991f9ddd9de2,BouncyCastle@r1rv50
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Maintainer Notes:
0.1.3,9 uses precompiled open-source jars that have been verified to match
hashes from upstream. An upcomming 0.1.4 should be build according to the
following recipe:

Build:0.1.4,10
    disable=not released, works for 0.1.3
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    commit=0.1.4
    submodules=yes
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        mvn clean && mvn package && popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/

Remove notice in the description as well, if 0.1.4 is build from completly from
source. 0.1.3 should be disabled after some time afterwards.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.2-alpha
Current Version Code:10

