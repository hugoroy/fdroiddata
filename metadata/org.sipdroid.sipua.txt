Categories:Phone & SMS
License:GPLv3
Web Site:http://sipdroid.org
Source Code:https://code.google.com/p/sipdroid/source
Issue Tracker:https://code.google.com/p/sipdroid/issues

Auto Name:Sipdroid
Summary:A SIP (VOIP) client
Description:
A SIP (VOIP) client with video calling capabilities. Visit the website for more
info.
For optimal battery usage reserve a free VoIP PBX on pbxes.org, and manage your
SIP trunks using a web browser.

We can't build this from source and will remove it soon if that can't be done.
Recent developer builds can be obtained from the website.
.

Repo Type:git-svn
Repo:https://sipdroid.googlecode.com/svn/trunk

Build:3.4 beta,97
    commit=675
    target=android-17
    buildjni=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.5 beta
Current Version Code:98

