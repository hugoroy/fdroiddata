Categories:Internet
License:GPLv2
Web Site:https://code.google.com/p/weather-notification-android
Source Code:https://code.google.com/p/weather-notification-android/source
Issue Tracker:https://code.google.com/p/weather-notification-android/issues

Name:Weather Skin: White
Auto Name:Weather notification skin: white text
Summary:Theme for Weather Notification
Description:
White skin for [[ru.gelin.android.weather.notification]].
.

Repo Type:srclib
Repo:WeatherNotification

Build:0.3-beta1,9
    commit=a717e37fca61
    subdir=skins/white-text
    target=android-15
    update=.,../../libs/libpreference,../../libs/libweather,../../libs/libweatherskin

Build:0.3,11
    commit=b4f537a97fe7
    subdir=skins/white-text
    target=android-15
    update=.,../../libs/libpreference,../../libs/libweather,../../libs/libweatherskin

Build:0.3.1,12
    commit=30d71fd307ae
    subdir=skins/white-text
    target=android-15
    update=.,../../libs/libpreference,../../libs/libweather,../../libs/libweatherskin

Build:0.3.3,14
    commit=a0c2e640e60a
    subdir=skins/white-text
    target=android-15
    forceversion=yes
    forcevercode=yes

Build:0.3.5,15
    commit=v.0.3.5
    subdir=skins/white-text

Auto Update Mode:Version v.%v
Update Check Mode:Tags
Current Version:0.3.5
Current Version Code:15

